<?php defined('ABSPATH') OR die('restricted access');

if ( ! function_exists( 'stamina_load_fonts' ) )
{
    function stamina_load_fonts()
    {
        $fonts_url = '';

        /* Translators: If there are characters in your language that are not
        * supported by Lato, translate this to 'off'. Do not translate
        * into your own language.
        */
        $lato = _x( 'on', 'Lato font: on or off', 'stamina' );
        $open_sans = _x( 'on', 'Open Sans font: on or off', 'stamina' );

        if ( 'off' !== $lato || 'off' !== $open_sans ) {

            $font_families = array();

            if ( 'off' !== $lato ) {
                $font_families[] = 'Lato:300,700,900';
            }

            if ( 'off' !== $open_sans  ) {
                $font_families[] = 'Open+Sans';
            }

            $urlencode = implode( '|', $font_families );

            $query_args = array(
                'family' => $urlencode,
            );

            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }

        return esc_url_raw( $fonts_url );
    }
}