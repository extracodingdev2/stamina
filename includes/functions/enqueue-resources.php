<?php defined('ABSPATH') OR die('restricted access');

/**
 * Enqueue scripts and styles.
 */
function stamina_theme_scripts() {

    if ( is_admin() ) {
        return null;
    }

    wp_enqueue_style( 'stamina-fonts', stamina_load_fonts() );

    // Stamina Load Css Files
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css' );
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css' );
    wp_enqueue_style( 'owl.carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css' );
    wp_enqueue_style( 'owl.transitions', get_template_directory_uri() . '/assets/css/owl.transitions.css' );
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css' );

    wp_enqueue_style( 'stamina-style', get_stylesheet_uri() );
    wp_enqueue_style( 'stamina-blog', get_template_directory_uri() . '/assets/css/blog.css' );
    wp_enqueue_style( 'stamina-responsive', get_template_directory_uri() . '/assets/css/responsive.css' );

    // Stamina Load Js Files
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'owl.carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'bootstrap' ) );
    wp_enqueue_script('mixitup', get_template_directory_uri() . '/assets/js/jquery.mixitup.js', array('jquery', 'bootstrap' ) );
    wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js' );
    wp_enqueue_script('waypoints', get_template_directory_uri() . '/assets/js/jquery.waypoints.min.js' );
    wp_enqueue_script('inview', get_template_directory_uri() . '/assets/js/jquery.inview.min.js' );
    wp_enqueue_script( 'stamina-scripts', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery' ), '', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

add_action( 'wp_enqueue_scripts', 'stamina_theme_scripts' );