/* ----------------------------------------------------------------------------------------
* Author        : Designcarebd
* Template Name : Stamina | One Page Multipurpose Html Template
* File          : Stamina main JS file
* Version       : 1.0
* ---------------------------------------------------------------------------------------- */


/* INDEX
----------------------------------------------------------------------------------------

01. Preloader js

02. change Menu background on scroll js

03. Navigation js

04. Smooth scroll to anchor

05. Slider auto paly script

06. Portfolio js

07. Magnific Popup js

08. Testimonial js

09. client js

10. Scroll-to-top

11. Counter  js

-------------------------------------------------------------------------------------- */


(function($) {
'use strict';

        var stamina = $(window);

    /*-------------------------------------------------------------------------*
     *                  01. Preloader js                                       *
     *-------------------------------------------------------------------------*/
        stamina.on( 'load', function(){

          $('#preloader').delay(300).fadeOut('slow',function(){
            $(this).remove();
          });

        }); // $(window).on end



    /*-------------------------------------------------------------------------*
     *             02. change Menu background on scroll js                     *
     *-------------------------------------------------------------------------*/
        stamina.on('scroll', function () {
          var menu_area = $('.menu-area');
          if (stamina.scrollTop() > 0) {
              menu_area.addClass('sticky-menu');
          } else {
              menu_area.removeClass('sticky-menu');
          }
        }); // $(window).on('scroll' end



    /*-------------------------------------------------------------------------*
     *                   03. Navigation js                                     *
     *-------------------------------------------------------------------------*/
        $(document).on('click', '.navbar-collapse.in', function (e) {
          if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
              $(this).collapse('hide');
          }
        });

        $('body').scrollspy({
          target: '.navbar-collapse',
          offset: 195
        });



    /*-------------------------------------------------------------------------*
     *                   04. Smooth scroll to anchor                           *
     *-------------------------------------------------------------------------*/
        $('a.smooth_scroll').on("click", function (e) {
          e.preventDefault();
          var anchor = $(this);
          $('html, body').stop().animate({
              scrollTop: $(anchor.attr('href')).offset().top - 50
          }, 1000);
        });



    /*-------------------------------------------------------------------------*
     *                  05. Slider Auto play js                                       *
     *-------------------------------------------------------------------------*/
        $('.carousel').carousel({
            interval: 4000
         })



    /*-------------------------------------------------------------------------*
     *                  06. Portfolio js                                       *
     *-------------------------------------------------------------------------*/
        $('.portfolio').mixItUp();



    /*-------------------------------------------------------------------------*
     *                  07. Magnific Popup js                                  *
     *-------------------------------------------------------------------------*/
        $('.work-popup').magnificPopup({
          type: 'image',
          gallery: {
              enabled: true
          },
          zoom: {
              enabled: true,
              duration: 300, // don't foget to change the duration also in CSS
              opener: function(element) {
                  return element.find('img');
              }
          }

        });


        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });



    /*-------------------------------------------------------------------------*
     *                  07. Testimonial js                                     *
     *-------------------------------------------------------------------------*/
      $(".testimonial-list").owlCarousel({
        singleItem      : true,
        lazyLoad        : false,
        pagination      : false,
        navigation      : false,
        autoPlay        : true,
      });



    /*-------------------------------------------------------------------------*
     *                       9. client js                                     *
     *-------------------------------------------------------------------------*/
        $(".owl-client").owlCarousel({
          items               : 4,
          autoPlay            : true,
          itemsDesktop        : [1199, 4],
          itemsDesktopSmall   : [980, 3],
          itemsTablet         : [768, 3],
          itemsMobile         : [479, 2],
          pagination          : false,
          navigation          : false,
          autoHeight          : true,
        });


        $(".news-carousel").owlCarousel({
          items               : 3,
          autoPlay            : false,
          itemsDesktop        : [1199, 3],
          itemsDesktopSmall   : [980, 2],
          itemsTablet         : [768, 2],
          itemsMobile         : [479, 1],
          pagination          : false,
          navigation          : true,
          navigationText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
      });

      $(".owl-slider").owlCarousel({
          items               : 1,
          itemsDesktop        : [1199, 1],
          itemsDesktopSmall   : [980, 1],
          itemsTablet         : [768, 1],
          itemsMobile         : [479, 1],
          pagination          : false,
          navigation          : true,
          navigationText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
      });



    /*-------------------------------------------------------------------------*
     *                       10. Scroll-to-top                           *
     *-------------------------------------------------------------------------*/
        stamina.on( 'scroll', function () {
            var top_top = $('#scroll-to-top');
            if (stamina.scrollTop() > 500) {
                top_top.fadeIn();
            } else {
                top_top.fadeOut();
            }
        });
        /*END SCROLL TO TOP*/



    /*-------------------------------------------------------------------------*
     *                       11. Counter  js                         *
     *-------------------------------------------------------------------------*/
        $('.fun-facts-area').on('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $(this).find('.timer').each(function () {
                    var $this = $(this);
                    $({ Counter: 0 }).animate({ Counter: $this.text() }, {
                        duration: 2000,
                        easing: 'swing',
                        step: function () {
                            $this.text(Math.ceil(this.Counter));
                        }
                    });
                });
                $(this).off('inview');
            }
        });



})(jQuery);