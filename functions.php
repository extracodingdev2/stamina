<?php defined('ABSPATH') OR die('restricted access');

// Theme Setup
require get_parent_theme_file_path( '/includes/functions/theme-setup.php' );

// Register and Enqueue theme resources
require get_parent_theme_file_path( '/includes/functions/enqueue-resources.php' );

// Theme Functions
require get_parent_theme_file_path( '/includes/functions/theme-functions.php' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function stamina_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'stamina' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'stamina' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'stamina_widgets_init' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/inc/jetpack.php';
}

